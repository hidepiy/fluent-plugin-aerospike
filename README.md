# Fluent::Plugin::Aerospike

Fluent BufferedOutput plugin for Aerospike

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'fluent-plugin-aerospike'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install fluent-plugin-aerospike

## Configuration
config             | default         | info
-------------------|-----------------|-----
address            | 127.0.0.1:3000  | address of Aerospike
namespace          | test            | namespace name of Aerospike
set                | fluent ${tag}   | set name of Aerospike
ttl                | 0               | time-to-live: If ttl = 0, the default ttl specified on the server side will be applied
time_key           |                 | this time_key field or time of record is used
time_format        |                 | (with time_key) the value of time_key will be parsed with this format

## Samples
### sample: access log
config

    <match ***>
      type aerospike
      time_key request_time
      time_format %d/%b/%Y:%H:%M:%S %Z
    </match>

log

    {"request_time":"24/Dec/2014:23:59:59 +0700","code":"100","response_time":"1000","site":"git"}

Aerospike

    aql> select * from test
    +---------------+------------------------------+-------+---------------+
    | site          | request_time                 | code  | response_time |
    +---------------+------------------------------+-------+---------------+
    | "git"         | "24/Dec/2014:23:59:59 +0700" | "100" | "1000"        |
    +---------------+------------------------------+-------+---------------+

## Contributing

1. Fork it ( https://github.com/[my-github-username]/fluent-plugin-aerospike/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

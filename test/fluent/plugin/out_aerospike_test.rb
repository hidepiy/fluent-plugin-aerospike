require 'helper'

class DataCounterOutputTest < Test::Unit::TestCase
  def setup
    Fluent::Test.setup
  end

  DEFAULT_CONFIG = %[
  ]

  DEFAULT_TAG = 'default_tag'

  def create_driver(conf = DEFAULT_CONFIG, tag = DEFAULT_TAG)
    Fluent::Test::BufferedOutputTestDriver.new(Fluent::AerospikeOutput, tag).configure(conf)
  end

  def test_configure_default
    d = create_driver
    assert_equal '127.0.0.1:3000', d.instance.address
    assert_equal 'test', d.instance.namespace
    assert_equal nil, d.instance.set
    assert_equal nil, d.instance.time_key
    assert_equal nil, d.instance.time_format
    assert_equal 0, d.instance.ttl
  end

  def test_configure
    d = create_driver(DEFAULT_CONFIG + %[
      address aerosmith:1234
      namespace rock
      set boston
      ttl 1
    ])
    assert_equal 'aerosmith:1234', d.instance.address
    assert_equal 'rock', d.instance.namespace
    assert_equal 'boston', d.instance.set
    assert_equal 1, d.instance.ttl
  end

  def test_format
    d = create_driver
    time = Time.parse("2112-09-03 01:23:45 UTC")
    d.emit({"gerogero" => "Let's get Rocking!", "site" => "yapoo"}, time)
    d.emit({"gerogero" => "Let's get Rocking!", "site" => "geegero"}, time)
    d.expect_format "\x93\xABdefault_tag\xCF\x00\x00\x00\x01\f[\xC8\xA1\x82\xA8gerogero\xB2Let's get Rocking!\xA4site\xA5yapoo\x93\xABdefault_tag\xCF\x00\x00\x00\x01\f[\xC8\xA1\x82\xA8gerogero\xB2Let's get Rocking!\xA4site\xA7geegero"
    # d.run

    # time = Time.parse("2011-01-02 13:14:15 UTC").to_i
    # d.emit({"a"=>1}, time)
    # d.emit({"a"=>2}, time)

    # d.expect_format %[2011-01-02T13:14:15Z\ttest\t{"a":1}\n]
    # d.expect_format %[2011-01-02T13:14:15Z\ttest\t{"a":2}\n]

    # d.run
  end

  def test_write
    d = create_driver

    # time = Time.parse("2011-01-02 13:14:15 UTC").to_i
    # d.emit({"a"=>1}, time)
    # d.emit({"a"=>2}, time)

    # ### FileOutput#write returns path
    # path = d.run
    # expect_path = "#{TMP_DIR}/out_file_test._0.log.gz"
    # assert_equal expect_path, path
  end
end

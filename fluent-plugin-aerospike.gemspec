# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "fluent-plugin-aerospike"
  spec.version       = "0.0.0"
  spec.authors       = ["Hatayama Hideharu"]
  spec.email         = ["h.hiddy@gmail.com"]
  spec.description   = %q{Fluent BufferedOutput plugin for Aerospike}
  spec.summary       = spec.description
  spec.homepage      = "https://bitbucket.org/hidepiy/fluent-plugin-aerospike"
  spec.license       = "APLv2"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_runtime_dependency "fluentd", "~> 0"
  spec.add_dependency "aerospike", "0.1.3"
  spec.add_dependency "uuidtools", "~> 2.1", ">= 2.1.5"
end

